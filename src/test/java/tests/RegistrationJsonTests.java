package tests;

import com.fasterxml.jackson.databind.ObjectMapper;
import models.RegistrationModel;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pageobjects.RegistrationPage;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import static org.testng.Assert.assertEquals;

public class RegistrationJsonTests extends BaseUITests {


    @DataProvider(name = "jsonDp")
    public Iterator<Object[]> jsonDpCollection() throws IOException {
        Collection<Object[]> dp = new ArrayList<>();
//      here is starting deserialization of json into Login object
//        ObjectMapper mapper = new ObjectMapper();
        ObjectMapper objectMapper = new ObjectMapper();

        File f = new File("src\\test\\resources\\data\\testdata.json");
//        LoginModel lm = mapper.readValue(f, LoginModel.class);
        RegistrationModel[] lms = objectMapper.readValue(f, RegistrationModel[].class);

        for (RegistrationModel lm : lms)
            dp.add(new Object[]{lm});

        return dp.iterator();
    }

    @Test(dataProvider = "jsonDp")
    public void registrationWithJsonTest(RegistrationModel lm) {
        printData(lm);
        registrationActions(lm);
    }

    private void printData(RegistrationModel lm) {
        System.out.println("Account.titulatura:" + lm.getAccount().getTitulatura());
        System.out.println("Account.nume:" + lm.getAccount().getNume());
        System.out.println("Account.prenume:" + lm.getAccount().getPrenume());
        System.out.println("Account.telefon:" +lm.getAccount().getTelefon());
        System.out.println("Account.email:" + lm.getAccount().getEmail());
        System.out.println("Account.parola:" + lm.getAccount().getParola());
        System.out.println("Account.parolaverif:"+lm.getAccount().getParolaVerif());
        System.out.println("Account.termreq: " + lm.getAccount().isTermreq());

        System.out.println("================REGISTRATION FIELD ERRORS==============");
        System.out.println("Titulatura error:" + lm.getTitulaturaError());
        System.out.println("nume error:" + lm.getNumeError());
        System.out.println("prenume error:" + lm.getPrenumeError());
        System.out.println("telefon error:" + lm.getTelefonError());
        System.out.println("email error:" + lm.getEmailError());
        System.out.println("parola error:" + lm.getParolaError());
        System.out.println("parolaVerif error:" + lm.getParolaVerifError());
        System.out.println("termeni error:" + lm.getTermreqError());
    }

    public void registrationActions(RegistrationModel lm){
        RegistrationPage registrationPage = new RegistrationPage(driver);
        registrationPage.openRegistrationPage(hostname);

        registrationPage.enterRegisterInputs(lm.getAccount().getTitulatura(), lm.getAccount().getNume(), lm.getAccount().getPrenume(),
                lm.getAccount().getTelefon(), lm.getAccount().getEmail(), lm.getAccount().getParola(), lm.getAccount().getParolaVerif(),
                lm.getAccount().isTermreq());

        registrationPage.performSubmit();
        String expectedTitulaturaErr = lm.getTitulaturaError();
        String expectednumeErr = lm.getNumeError();
        String expectedprenumeErr = lm.getPrenumeError();
        String expectedtelefonErr = lm.getTelefonError();
        String expectedemailErr = lm.getEmailError();
        String expectedparolaErr = lm.getParolaError();
        String expectedparolaVerifErr = lm.getParolaVerifError();
        String expectetermreqErr = lm.getTermreqError();


        assertEquals(registrationPage.getTitulaturaErr(),expectedTitulaturaErr);
        assertEquals(registrationPage.getNumeErr(), expectednumeErr);
        assertEquals(registrationPage.getPrenumeErr(), expectedprenumeErr);
        assertEquals(registrationPage.getTelefonErr(), expectedtelefonErr);
        assertEquals(registrationPage.getEmailErr(), expectedemailErr);
        assertEquals(registrationPage.getParolaErr(), expectedparolaErr);
        assertEquals(registrationPage.getVerifyParolaErr(), expectedparolaVerifErr);
        assertEquals(registrationPage.getThermsErr(), expectetermreqErr);
    }
}
