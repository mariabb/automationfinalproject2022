package tests;

import org.openqa.selenium.WebDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import utils.SeleniumUtils;

import java.io.IOException;
import java.util.Properties;

public class BaseUITests {

    WebDriver driver;
    String hostname;
    //        String username;
//        String password;
    String browser;

    @AfterMethod(alwaysRun = true)
    public void close() {
//      inchide browser-ul
        if (driver != null) {
             driver.quit();
        }
    }

    @BeforeMethod
    public void setUpBasics() throws IOException {
        Properties prop = SeleniumUtils.readProperties("src\\test\\resources\\framework.properties");
//    try to get browser from command line
        browser = System.getProperty("browser");
//      get default value
        if (browser == null)
            browser = prop.getProperty("browser");

        System.out.println("Use browser:" + browser);

        //read default values from config file
        hostname = prop.getProperty("hostname");
        System.out.println("Use next hostname:" + hostname);
        driver = SeleniumUtils.getDriver(browser);
    }


    @AfterMethod
    public void saveScreenShotAtFailure(ITestResult result) {
        if (result.getStatus() == ITestResult.FAILURE) {
            //your screenshooting code goes here
            String testName = result.getMethod().getMethodName();
            System.out.println("Take screen shoot..for   testname: " + testName);
            SeleniumUtils.takeScreenshot(driver, testName);
        }
    }
}


