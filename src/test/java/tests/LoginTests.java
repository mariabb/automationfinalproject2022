package tests;

import dataproviders.DataProviders;
import models.LoginModel;
import org.testng.Assert;
import org.testng.annotations.Test;
import pageobjects.LoginPageBiogama;

import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;


public class LoginTests extends BaseUITests {

    @Test(dataProvider = "SQL_dataProviderNegativeData", dataProviderClass = DataProviders.class)
    public void testNegativeLoginAction(LoginModel lm) {
        LoginPageBiogama loginPageBiogama = new LoginPageBiogama(driver);
//       open login page
        loginPageBiogama.openLoginPage(hostname);
//         login
        loginPageBiogama.login(lm.getAccount().getEmail(), lm.getAccount().getPassword());
        System.out.println("Login button was pressed");

        String expectedEmailError = lm.getEmailError();
        String expectedPassErr = lm.getPasswordError();

        System.out.println("Verify expected errors present: expected emailError:" + expectedEmailError);
        System.out.println("Verify expected errors present: expected PasswordError:" + expectedPassErr);

      //  Assert.assertEquals(lm.getEmailError(), loginPageBiogama.getEmailError());
        Assert.assertEquals(expectedEmailError, loginPageBiogama.getEmailError());
        Assert.assertEquals(expectedPassErr, loginPageBiogama.getPasswordError());
     //   Assert.assertEquals(lm.getPasswordError(), loginPageBiogama.getPasswordError());

//        System.out.println("Verify expected errors present:\n expected userError:" + expectedUsernameErr);
//        Assert.assertTrue(loginPageBiogama.checkErr(expectedUsernameErr, "userErr"));
//
//        System.out.println("Expected Password Err:" + expectedPassErr);
//        Assert.assertTrue(loginPageBiogama.checkErr(expectedPassErr, "passErr"));

//        Assert.assertEquals(lm.getEmailError(), loginPageBiogama.getEmailError(), "ID: " + lm.getId());
//        Assert.assertEquals(lm.getPasswordError(), loginPageBiogama.getPasswordError(), "ID: " + lm.getId());
    }

    private void printData(LoginModel lm) {
        System.out.println("Account:username" + lm.getAccount().getEmail()
                + "/password:" + lm.getAccount().getPassword());
        System.out.println("User error:" + lm.getEmailError());
        System.out.println("Password error:" + lm.getPasswordError());
    }

    @Test(dataProvider = "positiveLoginDataProviderBiogama", dataProviderClass = DataProviders.class)
    public void testPositiveLogin(String email, String password, String modifDateInput) {
        LoginPageBiogama loginPageBiogama = new LoginPageBiogama(driver);
        loginPageBiogama.openLoginPage(hostname);
        System.out.println("Complete LogIn fields with valid data");
        loginPageBiogama.login(email, password);
        loginPageBiogama.waitForVoucherToBeDisplayed();
        System.out.println("Login button was pressed");

        Assert.assertEquals(modifDateInput, loginPageBiogama.getModifDateHeaderText());
    }
}
//INITIAL
//    @Test(dataProvider = "negativeLoginDataProviderBiogama", dataProviderClass = LoginBiogamaTests.class)
//    public void testNegativeLogin(String browserType, String email, String password,
//                                  String expectedUsernameErr, String expectedPassErr) {
//        LoginPageBiogama loginPageBiogama = new LoginPageBiogama(driver);
//
////       open login page
//        loginPageBiogama.openLoginPage(hostname);
//
////         login
//        loginPageBiogama.login(email, password);
//        System.out.println("Login button was pressed");
//
//        Assert.assertTrue(loginPageBiogama.checkErr(expectedUsernameErr, "userErr"));
//        Assert.assertTrue(loginPageBiogama.checkErr(expectedPassErr, "passErr"));
//    }
