package tests;

import dataproviders.DataProviders;
import models.Produs;
import org.testng.Assert;
import org.testng.annotations.Test;
import pageobjects.FavoritesPage;
import pageobjects.HomePage;
import pageobjects.LoginPageBiogama;
import pageobjects.SearchPage;

public class AddToFavoritesTests extends BaseUITests {
    @Test(dataProvider = "positiveAddToFavorites", dataProviderClass = DataProviders.class)
    public void testAddToFavorites(String email, String password, String numeProdusCautat) throws InterruptedException {
        LoginPageBiogama loginPageBiogama = new LoginPageBiogama(driver);
        loginPageBiogama.openLoginPage(hostname);
        loginPageBiogama.login(email, password);
        loginPageBiogama.waitForVoucherToBeDisplayed();

        System.out.println("Login button was pressed");
        HomePage homePage = new HomePage(driver);
        homePage.goToFavorites();

        FavoritesPage favoritesPage = new FavoritesPage(driver);
        favoritesPage.storeProductElementsInMemory();
        favoritesPage.deleteProductFromFavorites(numeProdusCautat);

        SearchPage searchPage = new SearchPage(driver);
        searchPage.searchProduct(numeProdusCautat);

        Produs produsCautat = searchPage.getFirstFoundProduct(numeProdusCautat);
        Assert.assertTrue(produsCautat.getNume().contains(numeProdusCautat));

        searchPage.addFirstFoundProductToWishList(numeProdusCautat);
        searchPage.goToFavorites();

        favoritesPage = new FavoritesPage(driver);
        favoritesPage.storeProductElementsInMemory();
        Produs produsFavorit = favoritesPage.getProdus(numeProdusCautat);

        Assert.assertTrue(produsFavorit.getNume().toLowerCase().contains(numeProdusCautat.toLowerCase()));
    }
}
