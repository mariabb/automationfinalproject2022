package tests;

import dataproviders.DataProviders;
import models.Produs;
import org.testng.Assert;
import org.testng.annotations.Test;
import pageobjects.LoginPageBiogama;
import pageobjects.SearchPage;

public class SearchTests extends BaseUITests {
    @Test(dataProvider = "positiveSearchProduct", dataProviderClass = DataProviders.class)
    public void testSearchProduct(String email, String password, String produs) throws InterruptedException {
        LoginPageBiogama loginPageBiogama = new LoginPageBiogama(driver);
        loginPageBiogama.openLoginPage(hostname);
        loginPageBiogama.login(email, password);
        loginPageBiogama.waitForVoucherToBeDisplayed();

        System.out.println("Login button was pressed");
        SearchPage searchPage = new SearchPage(driver);
        searchPage.searchProduct(produs);

        Produs produsCautat = searchPage.getFirstFoundProduct(produs);
        Assert.assertTrue(produsCautat.getNume().contains(produs));
    }
}
