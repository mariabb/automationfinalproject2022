package tests;

import dataproviders.DataProviders;
import models.Produs;
import org.testng.Assert;
import org.testng.annotations.Test;
import pageobjects.*;

public class AddToCartTests extends BaseUITests {
    @Test(dataProvider = "positiveAddToCart", dataProviderClass = DataProviders.class)
    public void testAddToCart(String email, String password, String numeProdusCautat) {
        LoginPageBiogama loginPageBiogama = new LoginPageBiogama(driver);
        loginPageBiogama.openLoginPage(hostname);
        loginPageBiogama.login(email, password);
        loginPageBiogama.waitForVoucherToBeDisplayed();

        System.out.println("Login button was pressed");
        HomePage homePage = new HomePage(driver);

        SearchPage searchPage = new SearchPage(driver);
        searchPage.searchProduct(numeProdusCautat);

        Produs produsCautat = searchPage.getFirstFoundProduct(numeProdusCautat);
        Assert.assertTrue(produsCautat.getNume().contains(numeProdusCautat));

        searchPage.addFirstFoundProductToCart(numeProdusCautat);
        homePage.goToCart();

        CartPage cartPage = new CartPage(driver);
        Produs produsInCos = cartPage.getProduct(numeProdusCautat);
        Assert.assertTrue(produsInCos.getNume().toLowerCase().contains(numeProdusCautat.toLowerCase()));
    }
}
