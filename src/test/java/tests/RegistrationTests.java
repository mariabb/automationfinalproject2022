package tests;

import com.fasterxml.jackson.databind.ObjectMapper;
import models.LoginModel;
import models.RegistrationModel;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pageobjects.RegistrationPage;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.concurrent.TimeUnit;

import static org.testng.Assert.assertEquals;

public class RegistrationTests extends BaseUITests {
    RegistrationPage registrationPage;

    @BeforeMethod
    public void initializePages() {
        registrationPage = new RegistrationPage(driver);
        registrationPage.openRegistrationPage(hostname);
    }

    @DataProvider(name = "negativeRegistrationDataProviderBiogama")
    public Object[][] negativeregistrationDataProviderBiogama() {
        return new Object[][]{
                {"-", "Alegeti titulatura", "", "Va rugam completati numele", "", "Va rugam completati prenumele",
                        "", "Numarul dumneavoastra de telefon", "", "Adresa dumneavoastra de e-mail", "", "Va rugam alegeti o parola",
                        "", "Va rugam confirmati parola", false, "Trebuie sa fiti de acord cu termenii si conditiile acestui website"},
                {"Dna.", "", "nume", "", "prenume", "", "0000", "Numar de telefon incorect","123@", "Adresa de e-mail nu este corecta", "1", "Parola trebuie sa aiba cel putin 4 caratere",
                        "2", "Parola trebuie sa fie identica in ambele campuri", false, "Trebuie sa fiti de acord cu termenii si conditiile acestui website"}
        };
    }

    @Test(dataProvider = "negativeRegistrationDataProviderBiogama")
    public void testNegativeRegistration(String titulatura, String titulaturaError, String nume, String numeError,
                                         String prenume, String prenumeError, String telefon, String telefonError, String email, String emailError, String parola,
                                         String parolaError, String parolaVerif, String parolaVerifError, boolean isAccepted, String termeregError) {

        registrationPage.enterRegisterInputs(titulatura, nume, prenume, telefon, email, parola, parolaVerif, isAccepted);
        registrationPage.performSubmit();
        assertEquals(registrationPage.getTitulaturaErr(), titulaturaError);
        assertEquals(registrationPage.getNumeErr(), numeError);
        assertEquals(registrationPage.getPrenumeErr(), prenumeError);
        assertEquals(registrationPage.getTelefonErr(), telefonError);
        assertEquals(registrationPage.getEmailErr(), emailError);
        assertEquals(registrationPage.getParolaErr(), parolaError);
        assertEquals(registrationPage.getVerifyParolaErr(), parolaVerifError);
        assertEquals(registrationPage.getThermsErr(), termeregError);
    }
}