package models;

public class LoginModel {
    private AccountModel account;
    private String emailError;
    private String passwordError;

    public LoginModel(String email, String password, String emailErr, String passwordError) {
        AccountModel ac = new AccountModel();
        ac.setEmail(email);
        ac.setPassword(password);

        this.account = ac;
        this.emailError = emailErr;
        this.passwordError = passwordError;
    }
    public AccountModel getAccount() {
        return account;
    }

    public void setAccount(AccountModel account) {
        this.account = account;
    }

    public String getEmailError() {
        return emailError;
    }

    public void setEmailError(String emailError) {
        this.emailError = emailError;
    }

    public String getPasswordError() {
        return passwordError;
    }

    public void setPasswordError(String passwordError) {
        this.passwordError = passwordError;
    }








//    public LoginModel() {
//    }



}


