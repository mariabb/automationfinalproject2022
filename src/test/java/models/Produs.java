package models;

public class Produs {
    private String nume;
    private String gramaj;
    private double pret;
    private String valuta;

    public Produs() {
    }

    public Produs(String nume, String gramaj, double pret, String valuta) {
        this.nume = nume;
        this.gramaj = gramaj;
        this.pret = pret;
        this.valuta = valuta;
    }

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public String getGramaj() {
        return gramaj;
    }

    public void setGramaj(String gramaj) {
        this.gramaj = gramaj;
    }

    public double getPret() {
        return pret;
    }

    public void setPret(double pret) {
        this.pret = pret;
    }

    public String getValuta() {
        return valuta;
    }

    public void setValuta(String valuta) {
        this.valuta = valuta;
    }
}
