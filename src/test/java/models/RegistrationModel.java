package models;

public class RegistrationModel {

    private AccountRegistrationModel account;
    private String titulaturaError;
    private String numeError;
    private String prenumeError;
    private String telefonError;
    private String emailError;
    private String parolaError;
    private String parolaVerifError;
    private String termreqError;

    public AccountRegistrationModel getAccount() {
        return account;
    }

    public void setAccount(AccountRegistrationModel account) {
        this.account = account;
    }

    public String getTitulaturaError() {
        return titulaturaError;
    }

    public void setTitulaturaError(String titulaturaError) {
        this.titulaturaError = titulaturaError;
    }

    public String getNumeError() {
        return numeError;
    }

    public void setNumeError(String numeError) {
        this.numeError = numeError;
    }

    public String getPrenumeError() {
        return prenumeError;
    }

    public void setPrenumeError(String prenumeError) {
        this.prenumeError = prenumeError;
    }

    public String getTelefonError() {
        return telefonError;
    }

    public void setTelefonError(String telefonError) {
        this.telefonError = telefonError;
    }

    public String getEmailError() {
        return emailError;
    }

    public void setEmailError(String emailError) {
        this.emailError = emailError;
    }

    public String getParolaError() {
        return parolaError;
    }

    public void setParolaError(String parolaError) {
        this.parolaError = parolaError;
    }

    public String getParolaVerifError() {
        return parolaVerifError;
    }

    public void setParolaVerifError(String parolaVerifError) {
        this.parolaVerifError = parolaVerifError;
    }

    public String getTermreqError() {
        return termreqError;
    }

    public void setTermreqError(String termreqError) {
        this.termreqError = termreqError;
    }



///"dropdownbyvisibilevalue": "text"



    public RegistrationModel(){

    }

    public RegistrationModel(String titulatura, String nume, String prenume, String telefon, String email, String parola,
                             String parolaVerif, boolean termreq, String titulaturaError, String numeError, String prenumeError, String
                                     telefonError, String emailError, String parolaError, String parolaVerifError, String termreqError){
        AccountRegistrationModel ac = new AccountRegistrationModel();

        ac.setTitulatura(titulatura);
        ac.setNume(nume);
        ac.setPrenume(prenume);
        ac.setTelefon(telefon);
        ac.setEmail(email);
        ac.setParola(parola);
        ac.setParolaVerif(parolaVerif);
        ac.setTermreq(termreq);

    }


}
