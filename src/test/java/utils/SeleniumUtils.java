package utils;


import io.github.bonigarcia.wdm.WebDriverManager;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Objects;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.TimeUnit;


public class SeleniumUtils {
    public static final String SCREEN_SHOOT_PATH = "src\\test\\resources\\screenshots";
    static DateTimeFormatter formatter = DateTimeFormatter.ISO_DATE_TIME;

    public static WebDriver getDriver(String browserType) {
        WebDriver driver = null;

        switch (Objects.requireNonNull(getBrowserEnumFromString(browserType))) {
            case CHROME:
                WebDriverManager.chromedriver().setup();
                driver = new ChromeDriver();
                break;
            case FIREFOX:
                WebDriverManager.firefoxdriver().setup();
                driver = new FirefoxDriver();
                break;
            case EDGE:
                WebDriverManager.edgedriver().setup();
                driver = new EdgeDriver();
                break;
            case IE:
                WebDriverManager.iedriver().setup();
                driver = new InternetExplorerDriver();
                break;
        }
        assert driver != null;
        driver.manage().window().maximize();
        return driver;
    }




    public static Browsers getBrowserEnumFromString(String browserType) {
        for (Browsers browser : Browsers.values()) {
            if (browserType.equalsIgnoreCase(browser.toString()))
                return browser;
        }
        System.out.println("Browser " + browserType + " is not on supported list");
        return null;
    }

    public static List<WebElement> waitForPresenceOfAllElementsGeneric(WebDriver driver, long timeOut, By by) {
        WebDriverWait wait = new WebDriverWait(driver, timeOut);
        return wait.until(
                ExpectedConditions.presenceOfAllElementsLocatedBy(by));
    }

    //read from properties files
    public static Properties readProperties(String path) throws IOException {
        InputStream inputStream = new FileInputStream(path);
        Properties properties = new Properties();
        properties.load(inputStream);
        return properties;
    }

    public static WebElement waitForPresenceOfElementGeneric(WebDriver driver, long timeOut, By by) {
        WebDriverWait wait = new WebDriverWait(driver, timeOut);
        return wait.until(
                ExpectedConditions.presenceOfElementLocated(by));
    }

    public static WebElement waitForPresenceOfElementToBeClickable(WebDriver driver, long timeOut, By by) {
        WebDriverWait wait = new WebDriverWait(driver, timeOut);
        wait.until(ExpectedConditions.presenceOfElementLocated(by));
        return wait.until(
                ExpectedConditions.elementToBeClickable(by));
    }

    public static void waitForModalToBeClosed(WebDriver driver, long timeOut, By by) {
        WebDriverWait wait = new WebDriverWait(driver, timeOut);
        wait.until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver webdriver) {
                return driver.findElements(by).size() == 0;
            }
        });

    }

    public static void takeScreenshot(WebDriver driver, String testName) {
        //Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        LocalDateTime currentDate = LocalDateTime.now();
        String dataTimeFormat = currentDate.format(formatter).split("\\.")[0].replaceAll(":", "");
        File screenShotFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        String screenShotName = SCREEN_SHOOT_PATH + "\\screenshot-" + testName + "-" + dataTimeFormat + ".jpeg";
        File saveFile = new File(screenShotName);
        System.out.println("Save screen shot at path:" + screenShotName);
        try {
            FileUtils.copyFile(screenShotFile, saveFile);
        } catch (IOException e) {
            System.out.println("Cannot save screen shots. Please investigate");
        }
    }

    public static void printCookies(WebDriver driver) {
        Set<Cookie> cookies = driver.manage().getCookies();
        if (cookies.size() > 0) {
            System.out.println("The number of cookies found:" + cookies.size());
            System.out.println("Cookie Name | Cookie Value | Cookie Expire Date");
            for (Cookie c : cookies) {
                if (c.getExpiry() != null)
                    System.out.println(c.getName() + "|" + c.getValue() + "|" + c.getExpiry());
            }
        } else
            System.out.println("No cookies present");
    }

    public static String getEscapedElement(ResultSet resultSet, String element) throws SQLException {
        return replaceElements(resultSet.getString(element), "''", "");
    }

    private static String replaceElements(String element, String valueToBeReplaced, String valueReplaceWith) {
        return element.replaceAll(valueToBeReplaced, valueReplaceWith);
    }

    public static Cookie getCookie(WebDriver driver, String name) {
        Set<Cookie> cookies = driver.manage().getCookies();
        for (Cookie c : cookies) {
            if (c.getName().equals(name)) {
                return c;
            }
        }
        return null;
    }

//    public static boolean isPresent(WebDriver,By by){
//        try {
//            driver.findElement(by);
//            return true;
//        }catch(NoSuchElementException noSuchElementException){
//            return false;
//        }
//    }

/*    public long waitUntilIdentifiedElementIsVisibleAndClickeble(By by) {
        waitUntilDemask(Timeouts.min2(), Timeouts.extraPoll()); long time = -System.currentTimeMillis(); try { Wait<WebDriver> wait = new FluentWait<>(driver) .withTimeout(300, TimeUnit.SECONDS) .pollingEvery(500, TimeUnit.MILLISECONDS) .ignoring(NoSuchElementException.class) .ignoring(NullPointerException.class) .ignoring(StaleElementReferenceException.class) .ignoring(TimeoutException.class) .ignoring(WebDriverException.class); wait.until(ExpectedConditions.and( ExpectedConditions.visibilityOfElementLocated(by), ExpectedConditions.presenceOfElementLocated(by), ExpectedConditions.elementToBeClickable(by))); } catch (Exception e) { checkForUnexpectedMessages(); screenshot.aufnehmenError(); String sb = "Das Element '" + by.toString() + "' wurde nicht gefunden"; throw new Zhp3WaitException(sb); } time = time + System.currentTimeMillis(); wartezeit = wartezeit + time; writeToCsv("wait;" + "istElementVorhanden", StringHelper.formatiereZeitInSekunden(time));
        return time;
    }*/
}
