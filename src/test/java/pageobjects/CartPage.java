package pageobjects;

import models.Produs;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import pageobjects.pageobjectmodel.ProdusCartRow;

import java.util.ArrayList;
import java.util.List;

public class CartPage extends HomePage {
    public CartPage(WebDriver driver) {
        super(driver);
    }

    List<ProdusCartRow> productCartElements = new ArrayList<>();

    @FindBy(xpath = "//table//tr//td[@class='bic']/parent::tr")
    private List<WebElement> cartElementRows;

    public Produs getProduct(String nume) {
        // parcurgem fiecare rand din favorite
        for (WebElement webElement : cartElementRows) {

            // luam randul si extragem elementul care are locator pentru nume
            WebElement numeElement = webElement.findElement(By.cssSelector("a"));
            productCartElements.add(new ProdusCartRow(numeElement, null));
            // luam numele produsului de pe locatoru de mai sus
            String numeProdus = numeElement.getText();
            // urmeaza sa verificam ca numele produslui ii similar cu numele expected
            if (numeProdus.toLowerCase().contains(nume.toLowerCase())) {
                // compunem obiectul care are numele cautat
                Produs produs = new Produs(numeProdus, null, 0, null);
                // returnam produsul gasit
                System.out.println("S-a gasit produsul cautat:" + nume);
                return produs;
            }
        }
        // daca nu a fost gasit nici un nume de produs, lista va fi goala si returnam null
        return null;
    }
}
