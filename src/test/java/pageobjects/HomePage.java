package pageobjects;

import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import utils.SeleniumUtils;

public class HomePage extends BasePage {
    public HomePage(WebDriver driver) {
        super(driver);
    }

    private final String closeVoucherSelector = "voucher_sticker_close";

    @FindBy(css = "div#contbox > a[href='https://www.biogama.ro/cont/favorite']")
    private WebElement favoriteProducts;

    @FindBy(id = closeVoucherSelector)
    private WebElement closeVoucher;

    @FindBy(css = "a[href='https://www.biogama.ro/cosul-meu']")
    private WebElement cartProducts;

    @FindBy(css = "a.cb-enable")
    private WebElement acceptCookies;

    public void goToFavorites() {
        favoriteProducts.click();
    }

    public void acceptCookies() {
        acceptCookies.click();
    }

    public void closeVoucher() {
        try {
            waitForVoucherToBeDisplayed();
            closeVoucher.click();
        } catch (TimeoutException |NoSuchElementException e) {
            System.out.println("Fereastra cu Voucher nu este afisata!");
        }
    }

    public void goToCart() {
        cartProducts.click();
    }


    public void waitForVoucherToBeDisplayed() {
        SeleniumUtils.waitForPresenceOfElementToBeClickable(driver, 1, By.id(closeVoucherSelector));
    }

}
