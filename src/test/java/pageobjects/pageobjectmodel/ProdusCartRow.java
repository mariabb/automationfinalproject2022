package pageobjects.pageobjectmodel;

import org.openqa.selenium.WebElement;

public class ProdusCartRow {
    private WebElement nume;
    private WebElement pret;

    public ProdusCartRow(WebElement nume, WebElement pret) {
        this.nume = nume;
        this.pret = pret;
    }

    public WebElement getNume() {
        return nume;
    }

    public void setNume(WebElement nume) {
        this.nume = nume;
    }

    public WebElement getPret() {
        return pret;
    }

    public void setPret(WebElement pret) {
        this.pret = pret;
    }
}
