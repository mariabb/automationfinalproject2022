package pageobjects.pageobjectmodel;

import org.openqa.selenium.WebElement;

public class ProdusResultBox {
    private WebElement nume;
    private WebElement pret;
    private WebElement addToCart;
    private WebElement addToFavorites;

    public ProdusResultBox(WebElement nume, WebElement pret, WebElement addToCart, WebElement addToFavorites) {
        this.nume = nume;
        this.pret = pret;
        this.addToCart = addToCart;
        this.addToFavorites = addToFavorites;
    }

    public WebElement getNume() {
        return nume;
    }

    public void setNume(WebElement nume) {
        this.nume = nume;
    }

    public WebElement getPret() {
        return pret;
    }

    public void setPret(WebElement pret) {
        this.pret = pret;
    }

    public WebElement getAddToCart() {
        return addToCart;
    }

    public void setAddToCart(WebElement addToCart) {
        this.addToCart = addToCart;
    }

    public WebElement getAddToFavorites() {
        return addToFavorites;
    }

    public void setAddToFavorites(WebElement addToFavorites) {
        this.addToFavorites = addToFavorites;
    }
}
