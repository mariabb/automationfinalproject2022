package pageobjects.pageobjectmodel;

import org.openqa.selenium.WebElement;

public class ProdusFavoriteRow {
    private WebElement nume;
    private WebElement pret;
    private WebElement addToCart;
    private WebElement deleteFavorit;

    public ProdusFavoriteRow(WebElement nume, WebElement pret, WebElement addToCart, WebElement deleteFavorit) {
        this.nume = nume;
        this.pret = pret;
        this.addToCart = addToCart;
        this.deleteFavorit = deleteFavorit;
    }

    public WebElement getNume() {
        return nume;
    }

    public void setNume(WebElement nume) {
        this.nume = nume;
    }

    public WebElement getPret() {
        return pret;
    }

    public void setPret(WebElement pret) {
        this.pret = pret;
    }

    public WebElement getAddToCart() {
        return addToCart;
    }

    public void setAddToCart(WebElement addToCart) {
        this.addToCart = addToCart;
    }

    public WebElement getDeleteFavorit() {
        return deleteFavorit;
    }

    public void setDeleteFavorit(WebElement deleteFavorit) {
        this.deleteFavorit = deleteFavorit;
    }
}
