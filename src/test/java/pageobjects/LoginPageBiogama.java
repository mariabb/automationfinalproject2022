package pageobjects;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import utils.SeleniumUtils;


public class LoginPageBiogama extends HomePage {
//        WebDriver driver;
//        WebDriverWait wait;


    @FindBy(xpath = "//input[@id='email']")
    WebElement email;

    //@FindBy(id = "input-login-password")
    @FindBy(id = "password")
    WebElement password;

    //    @FindBy(xpath = "//button[@type='submit']")
    @FindBy(xpath = "//input[@id='login']")
    WebElement buttonSubmit;

    @FindBy(id = "email-error")
    WebElement emailErr;
    @FindBy(id = "password-error")
    WebElement passwordErr;

    @FindBy(xpath = "//h1[contains(text(),'Modificare date cont')]")
    WebElement modificareDate;

    private final String modifDatesLocator = "//div[@class = 'row bigbox']//form[@id = 'modificarecont']/preceding-sibling::h1";

    @FindBy(xpath = modifDatesLocator)
    WebElement modifyDatesElement;

    @FindBy(xpath = "//div[@class = 'row bigbox']//form[@id = 'modificarecont']/following-sibling::h1")
    WebElement modifyPasswElement;

    @FindBy(xpath = "//input[@id='nume']")
    WebElement numeModifDate;

    @FindBy(xpath = "//div[@id='adresafacturare']")
    WebElement adrese;

    @FindBy(xpath = "//form[@id='modificarecont']/following-sibling::h1")
    WebElement modifParola;

    //h1[contains(text(),'Modificare parolă')]
    //div.row:nth-child(6) div.container-fluid:nth-child(1) div.col-md-9.col-sm-12 div.row.bigbox div.row div.col-md-6.col-xs-12.col-centered.form-group:nth-child(1) div:nth-child(2) > h4:nth-child(1)
    //h4[contains(text(),'Adrese')]
    // By passwordErrBy = By.xpath("//input[@id='input-login-password']/parent::div/div[@class='text-left invalid-feedback']");


    public LoginPageBiogama(WebDriver driver) {
        super(driver);
    }

    public void openLoginPage(String hostname) {
        System.out.println("Open the next url:" + hostname + "cont/login");
        driver.get(hostname + "cont/login");
    }

    //https://www.biogama.ro/cont/login
    public void login(String usernameInp, String passwordInput) {
        email.clear();
        email.sendKeys(usernameInp);
        password.clear();
        password.sendKeys(passwordInput);
        buttonSubmit.submit();
        acceptCookies();
        closeVoucher();
    }

    public void waitForVoucherToBeDisplayed(){
        SeleniumUtils.waitForPresenceOfElementToBeClickable(driver, 5, By.xpath(modifDatesLocator));
    }

//    public void waitForPasswordErr() {
//        passwordErr = SeleniumUtils.waitForPresenceOfElementGeneric(driver, 15, passwordErrBy);
//    }

    public String getEmailError() {
        try {
            return emailErr.getText();
        } catch (NoSuchElementException e) {
            return "";
        }
    }

    public String getPasswordError() {
        try {
            return passwordErr.getText();
        } catch (NoSuchElementException e) {
            return "";
        }
    }

    public String getModifDateHeaderText(){
        return modifyDatesElement.getText();
    }

    public String getModifParolaHeaderText(){
        return modifParola.getText();
    }
}