package pageobjects;

import models.Produs;
import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import pageobjects.pageobjectmodel.ProdusResultBox;

import java.util.ArrayList;
import java.util.List;

public class SearchPage extends HomePage {
    List<ProdusResultBox> productElements = new ArrayList<>();

    @FindBy(css = "input#cautare")
    private WebElement cauta;

    @FindBy(css = "div.row.bigboxin div.boxlist")
    private List<WebElement> produseRezultate;

    public SearchPage(WebDriver driver) {
        super(driver);
    }

    public void searchProduct(String produs) {
        // Tastam numele produsului in bara de cautare
        cauta.sendKeys(produs);
        // Apasam tasta enter
        cauta.sendKeys(Keys.ENTER);
    }

    public List<Produs> getProduseRezultate() {
        // Golim lista de rezultate
        productElements.clear();
        List<Produs> produse = new ArrayList<>();

        // bucla pentru a adauga fiecare produs gasit la lista de elemente si la lista de produse
        for (WebElement webElement : produseRezultate) {
            // Adaugam randul de web elemente la lista de web elemente
            // Luam elementul care contine numele
            WebElement numeElement = webElement.findElement(By.cssSelector("div.title a"));
            // Luam elementul care contine pretul
            WebElement pretElement = webElement.findElement(By.cssSelector("p.detalii strong"));
            // elementul care contine butonul de adaugare in cos (default null
            WebElement addToCartElement = null;
            try {
                // Daca e stoc epuizat nu avem buton de adauga in cos
                addToCartElement = webElement.findElement(By.cssSelector("span.addcosmic"));
            } catch (NoSuchElementException e) {
                System.out.println("Stoc epuizat");
            }

            // Adaugam elementul care contine butonul de add to wish list
            WebElement addToWishListElement = webElement.findElement(By.cssSelector("div[class *= fav]"));

            // Compunem obiectul care contine toate de mai sus
            ProdusResultBox produsResultBox = new ProdusResultBox(numeElement, pretElement, addToCartElement, addToWishListElement);
            productElements.add(produsResultBox);

            // Luam numele, pretul, gramaj, valuta produsului
            String nume = numeElement.getAttribute("title");
            double pret = Double.parseDouble(pretElement.getText().replace("lei", ""));
            String gramaj = webElement.findElement(By.cssSelector("div.title a span")).getText().split(" ")[1];
            String valuta = webElement.findElement(By.cssSelector("p.detalii span")).getText();

            // Compunem obiectul produs cu cele 4 de mai sus
            Produs produs = new Produs(nume, gramaj, pret, valuta);
            // il adaugam la lista de produse
            produse.add(produs);
        }
        // returnam lista de produse
        return produse;
    }

    public Produs getFirstFoundProduct(String nume) {
        List<Produs> produse = getProduseRezultate();
        for (Produs produs : produse) {
            if (produs.getNume().contains(nume)) {
                return produs;
            }
        }
        return null;
    }

    public void addFirstFoundProductToWishList(String nume) {
        getProduseRezultate();
        for (ProdusResultBox produs : productElements) {
            if (produs.getNume().getText().toLowerCase().contains(nume.toLowerCase())) {
                if (!produs.getAddToFavorites().getAttribute("class").contains("favon")) {
                    produs.getAddToFavorites().click();
                }
                break;
            }
        }
    }

    public void addFirstFoundProductToCart(String nume) {
        getProduseRezultate();
        for (ProdusResultBox produsBox : productElements) {
            if (produsBox.getNume().getText().toLowerCase().contains(nume.toLowerCase())) {
                if (produsBox.getAddToCart() != null) {
                    produsBox.getAddToCart().click();
                }
                break;
            }
        }
    }
}
