package pageobjects;

import models.Produs;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import pageobjects.pageobjectmodel.ProdusFavoriteRow;

import java.util.ArrayList;
import java.util.List;

public class FavoritesPage extends HomePage {
    List<ProdusFavoriteRow> productFavoriteElements = new ArrayList<>();

    public FavoritesPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(css = "table.favtbl tr:not(.hidden-lg)")
    // avem lista de randuri cu fiecare produs (care are nume, adauga la favorite, adauga in cos)
    private List<WebElement> productElementRows;

    public void storeProductElementsInMemory() {
        productFavoriteElements.clear();
        // parcurgem fiecare rand din favorite
        for (WebElement webElement : productElementRows) {
            // luam randul si extragem elementul care are locator pentru nume
            WebElement numeElement = webElement.findElement(By.cssSelector("a"));
            WebElement deleteFavoritElement = webElement.findElement(By.cssSelector("div.favon"));
            productFavoriteElements.add(new ProdusFavoriteRow(numeElement, null, null, deleteFavoritElement));
        }
    }

    public Produs getProdus(String nume) {
        for (ProdusFavoriteRow produsFavoriteRow : productFavoriteElements) {
            // luam numele produsului de pe locatoru de mai sus
            String numeProdus = produsFavoriteRow.getNume().getText();
            // urmeaza sa verificam ca numele produslui ii similar cu numele expected
            if (numeProdus.toLowerCase().contains(nume.toLowerCase())) {
                // compunem obiectul care are numele cautat
                Produs produs = new Produs(numeProdus, null, 0, null);
                // returnam produsul gasit
                System.out.println("S-a gasit produsul cautat:" + nume);
                return produs;
            }
        }
        // daca nu a fost gasit nici un nume de produs, lista va fi goala si returnam null
        return null;
    }

    public void deleteProductFromFavorites(String nume) {
        for (ProdusFavoriteRow produsFavoriteRow : productFavoriteElements) {
            // luam numele produsului de pe locatoru de mai sus
            String numeProdus = produsFavoriteRow.getNume().getText();
            // urmeaza sa verificam ca numele produslui ii similar cu numele expected
            if (numeProdus.toLowerCase().contains(nume.toLowerCase())) {
                Actions actions = new Actions(driver);
                actions.moveToElement(produsFavoriteRow.getDeleteFavorit()).perform();
                produsFavoriteRow.getDeleteFavorit().click();
                System.out.println("S-a gasit produsul cautat pentru stergere:" + nume);
            }
        }
    }

}
