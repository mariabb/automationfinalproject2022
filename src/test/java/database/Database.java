package database;

import models.LoginModel;
import utils.SeleniumUtils;

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Properties;

import static tests.BaseUITests.*;
import static utils.SeleniumUtils.getEscapedElement;

public class Database {
    public static String dbHostname, dbUser, dbSchema, dbPassword, dbPort;

    static {
        Properties prop = null;
        try {
            prop = SeleniumUtils.readProperties("src\\test\\resources\\framework.properties");
        } catch (IOException e) {
            e.printStackTrace();
        }
        // try to get browser from command line
        assert prop != null;
        dbHostname = prop.getProperty("dbHostname");
        System.out.println("Use dbHostname:" + dbHostname);
        dbUser = prop.getProperty("dbUser");
        System.out.println("Use dbUser:" + dbUser);
        dbPort = prop.getProperty("dbPort");
        System.out.println("Use dbPort:" + dbPort);
        dbPassword = prop.getProperty("dbPassword");
        System.out.println("Use dbPassword:" + dbPassword);
        dbSchema = prop.getProperty("dbSchema");
        System.out.println("Use dbSchema:" + dbSchema);
    }

    public void databaseSetup() throws IOException {
        // get DB connection settings

    }

    public static Iterator<Object[]> getNegativeLoginData() {
        Collection<Object[]> dp = new ArrayList<>();
        try {
            Connection connection = DriverManager.getConnection("jdbc:mysql://" + dbHostname + ":" + dbPort
                    + "/" + dbSchema, dbUser, dbPassword);
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery("SELECT * FROM login_negative");
            while (resultSet.next()) {
                LoginModel lm = new LoginModel( getEscapedElement(resultSet, "email"),
                        getEscapedElement(resultSet, "password"),
                        getEscapedElement(resultSet, "emailErr"),
                        getEscapedElement(resultSet, "passwordErr"));
                dp.add(new Object[]{lm});
            }
            connection.close();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return dp.iterator();
    }
}
