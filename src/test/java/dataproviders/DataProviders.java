package dataproviders;

import database.Database;
import org.testng.annotations.DataProvider;

import java.util.Iterator;

public class DataProviders {
    @DataProvider(name = "negativeLoginDataProviderBiogama")
    public static Object[][] negativeLoginDataProviderBiogama() {
        return new Object[][]{
                {"", "", "Adresa dumneavoastra de e-mail", "Va rugam completati parola"},
//                {"firefox", "test", "", "", "Va rugam completati parola"},
//                {"edge", "", "123TEst", "Adresa dumneavoastra de e-mail", ""},
//                {"chrome", "test", "TEsting23", "", ""}
        };

    }


    @DataProvider(name = "positiveLoginDataProviderBiogama")
    public static Object[][] positiveLoginDataProviderBiogama() {
        return new Object[][]{
                {"mariabufnea@gmail.com", "Engleza2021", "Modificare date cont"}
//                {"firefox", "test", "", "", "Va rugam completati parola"},
//                {"edge", "", "123TEst", "Adresa dumneavoastra de e-mail", ""},
//                {"chrome", "test", "TEsting23", "", ""}
        };
    }

    @DataProvider(name = "positiveAddToFavorites")
    public static Object[][] positiveAddToFavorites() {
        return new Object[][]{
                {"test3@mailsac.com", "Engleza2021", "Agar Agar fara gluten"}
        };
    }

    @DataProvider(name = "positiveSearchProduct")
    public static Object[][] positiveSearchProduct() {
        return new Object[][]{
                {"test3@mailsac.com", "Engleza2021", "Agar Agar fara gluten"}
        };
    }

    @DataProvider(name = "positiveAddToCart")
    public static Object[][] positiveAddToCart() {
        return new Object[][]{
                {"test3@mailsac.com", "Engleza2021", "Agar Agar fara gluten"}
        };
    }

    @DataProvider(name = "SQL_dataProviderNegativeData")
    public static Iterator<Object[]> dataProviderNegativeData() {
        return Database.getNegativeLoginData();
    }
}
